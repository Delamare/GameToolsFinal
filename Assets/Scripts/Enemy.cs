﻿using UnityEngine;
using System.Collections;

public class Enemy : Agent 
{
    [SerializeField]
    public static bool instakillOnTouch = true;

	public int damage = 1;

    void OnTriggerEnter2D(Collider2D other)
    {

        //other.gameObject.SendMessage("ModifyHealth", -MaxHealth, SendMessageOptions.DontRequireReceiver);
        //Destroy(this.gameObject);

		if (other.gameObject.tag == "Player") {
//            Debug.Log("You're dead bro!");
//            SendMessage("ModifyHealth", -MaxHealth, SendMessageOptions.DontRequireReceiver);
			other.gameObject.GetComponentInParent<Player> ().ModifyHealth (-damage);
			Vector2 force = Vector2.one * 10;
			other.GetComponent<CharacterController2D> ().velocity = other.transform.position.x > transform.position.x ? force : force * -1;
//			other.GetComponent<Rigidbody2D> ().AddForce (other.transform.position.x > transform.position.x ? force : force * -1, ForceMode2D.Impulse);
//            other.gameObject.SendMessage("ModifyHealth", -MaxHealth, SendMessageOptions.DontRequireReceiver);

//            Destroy(other.gameObject);
		} else if(!other.gameObject.name.Contains("OMG"))
			Destroy (other.gameObject);
    }
}
