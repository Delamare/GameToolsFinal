﻿using UnityEngine;
using System.Collections;

public class Boss : Enemy
{
	Vector2 movementDirection;

	public Bullet projectile;

	public float bulletSpeed = 1;
	public float movementSpeed = 1;
	public float changeDirectionRate = 1f;
	public float fireRate = 1f;
	public float offset = 0;
	public float jumpForce = 5;
	public float jumpRate = 1f;

	Rigidbody2D rigidbody;

	void Start()
	{
		InvokeRepeating ("ChangeDirection", 0f, changeDirectionRate);
		InvokeRepeating ("Fire", 0.25f, fireRate);
		InvokeRepeating ("Jump", 0.75f, jumpRate);
		rigidbody = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		rigidbody.AddForce (movementDirection, ForceMode2D.Force);
	}

	void ChangeDirection()
	{
		if(transform.position.x > GameManager.instance.omgs[0].transform.position.x)
			movementDirection = new Vector2 (Random.Range (-1, 2) * movementSpeed, 0f);
		else
			movementDirection = new Vector2 (movementSpeed, 0f);
	}

	void Jump()
	{
		rigidbody.AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
	}

	public void Heal()
	{
		SetHealth (baseHealth);
	}

	void Fire()
	{
		if (Random.Range (0, 2) == 0)
		{
			Bullet bullet = (Bullet)Instantiate (projectile, transform.position + (Vector3.left * offset), transform.rotation);
			bullet.GetComponent<Rigidbody2D>().AddForce(-Vector2.right * bulletSpeed * Time.deltaTime);
		}
	}

	protected override void OnDie ()
	{
		Application.LoadLevel ("Main");
		base.OnDie ();
	}
}
