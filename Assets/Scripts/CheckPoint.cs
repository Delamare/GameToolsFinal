﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		Player p = other.GetComponent<Player> ();
		if (p != null)
		{
			p.lastSpawnPoint = transform.position;
		}
	}
}
