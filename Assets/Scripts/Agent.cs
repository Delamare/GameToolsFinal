﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Agent : MonoBehaviour 
{
    public delegate void AgentChangedEvent();
    public AgentChangedEvent agentChanged;
    public AgentChangedEvent agentDied;

    [SerializeField] protected int baseHealth = 100;
    int maxHealth = 100;

	[HideInInspector]
	public int health;

    public int Health{get{return health;}}

    public int MaxHealth{get{return maxHealth;} set{ maxHealth = value;}}

    protected virtual void Awake()
    {
        health = baseHealth;
    }

    protected virtual void Start()
    {
        CallAgentChanged();
    }

    /// <summary>Modifies the health of the Agent. If the result is a negative health, the Agent dies and calls OnDie.</summary>
    /// <param name='amount'>Amount of health to change. Positive to add, negative to subtract.</param>
    public virtual void ModifyHealth(int amount)
    {
        health = Math.Min(health + amount, maxHealth);
        if(health <= 0)
        {
            OnDie ();
			return;
        }
    }

    void CallAgentChanged()
    {
        if (agentChanged != null)
        {
            agentChanged();
        }
    }

    protected virtual void OnDie()
    {
//		Debug.LogWarning ("OnDieCalled");
        Destroy (gameObject);
    }

    /// <summary> Sets the health of the Agent. Only use this if you're sure you need to.</summary>
    /// <param name='health'>Health to set the Agent to.</param>
    protected virtual void SetHealth(int health)
    {
        this.health = Math.Min(health, maxHealth);
        if(health <= 0)
        {
            OnDie();
        }
    }

    /// <summary>Kill the Agent.</summary>
    public virtual void Kill()
    {
        health = 0;
        CallAgentChanged();
        OnDie();
    }
}
