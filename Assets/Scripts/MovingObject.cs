﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingObject : MonoBehaviour 
{
    [SerializeField]
    private float timeToMove;
    private float tTimeToMove;
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private bool moveVertical;
    [SerializeField]
    private bool moveHorizontal;
    [SerializeField]
    private bool isGoingLeft = false;
    [SerializeField]
    private bool isGoingUp = false;

    Vector3 directionToMove = Vector3.zero;

    void FixedUpdate()
    {
        if (moveHorizontal)
        {
            if (isGoingLeft)
            {
                directionToMove = new Vector3(timeToMove, 0, 0).normalized;
                this.transform.position += -directionToMove * moveSpeed * Time.deltaTime;
                tTimeToMove++;
                // If gone too far, switch direction
                if (tTimeToMove >= timeToMove)
                    isGoingLeft = false;
            }
            else
            {
                directionToMove = new Vector3(-timeToMove, 0, 0).normalized;
                this.transform.position -= directionToMove * moveSpeed * Time.deltaTime;
                tTimeToMove--;
                // If gone too far, switch direction
                if (tTimeToMove <= 0)
                    isGoingLeft = true;
            }
        }
        if (moveVertical)
        {
            if (isGoingUp)
            {
                directionToMove = new Vector3(0, timeToMove, 0).normalized;
                this.transform.position += -directionToMove * moveSpeed * Time.deltaTime;
                tTimeToMove++;
                // If gone too far, switch direction
                if (tTimeToMove >= timeToMove)
                    isGoingUp = false;
            }
            else
            {
                directionToMove = new Vector3(0, -timeToMove, 0).normalized;
                this.transform.position -= directionToMove * moveSpeed * Time.deltaTime;
                tTimeToMove--;
                // If gone too far, switch direction
                if (tTimeToMove <= 0)
                    isGoingUp = true;
            }
        }
    }
}
