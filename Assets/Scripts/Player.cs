﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class AnimationDelay
{
    //Private
    [SerializeField]
    private float animationDelayTimer = 0.0f;
    //Public
    public float shootAnimationDelayTimer { get { return animationDelayTimer; } set { animationDelayTimer = value; } }
}

[System.Serializable]
public class KillBox
{
    //Private
    [SerializeField]
    private int killBox;
    //Public
    public int theKillBox { get { return killBox; } set { killBox = value; } }
}

[System.Serializable]
public class Spawner
{
    //Private
    [SerializeField]
    private int theSpawnOffSet;
    [SerializeField]
    private Transform theSpawnPoint;

    //Public
    public int spawnOffSet { get { return theSpawnOffSet; } set { theSpawnOffSet = value; } }
    public Transform spawnPoint { get { return theSpawnPoint; } set { theSpawnPoint = value; } }
}

#pragma warning disable 0114 //Warning about hiding base.agent in script
public class Player : Agent
{
    // movement config
    public float gravity = -25f;
    public float runSpeed = 8f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;
    public float jumpHeight = 3f;
    private float normalizedHorizontalSpeed = 0;

    private CharacterController2D _controller;
    private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;

    //animation timing
    private bool animationDelay;

    // input
    private bool _right;
    private bool _left;
    private bool _up;
    private bool _fire;
    private bool hasFired = false;
    private bool hasJumped;
    private bool lastDirectionLeft;

    [SerializeField]
    private bool isDoubleJump = false;
    [SerializeField]
    private float doubleJumpHeight;
    [SerializeField]
    private float fireRate;
    private float fireCooldown;
    [SerializeField]
    private int bulletSpeed = 500;
    [SerializeField]
    private Rigidbody2D bullet;
    [SerializeField]
    private float bulletOffset;

    //Derivatives 
    public KillBox killBox;
    public Spawner spawner;
    public AnimationDelay animationDelayTimer;

	// Trevor Berninger
	[HideInInspector]
	public Vector3 lastSpawnPoint;
	Slider healthSlider;

    void Awake()
    {
        base.Awake();
        hasFired = false;
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController2D>();

        // listen to some events for illustration purposes
        _controller.onControllerCollidedEvent += onControllerCollider;
        _controller.onTriggerEnterEvent += onTriggerEnterEvent;
        _controller.onTriggerExitEvent += onTriggerExitEvent;

        if (doubleJumpHeight == 0)
            doubleJumpHeight = jumpHeight / 2;

        if (spawner.spawnPoint == null)
            spawner.spawnPoint = this.transform;

        if (animationDelayTimer.shootAnimationDelayTimer == 0)
            animationDelayTimer.shootAnimationDelayTimer = 0.16f;

        if (bulletOffset == 0)
            bulletOffset = 0.45f;

        animationDelay = false;
//		healthSlider = GetComponentInChildren<Slider> ();
		healthSlider = FindObjectOfType<Slider>();
		if (healthSlider)
		{
			healthSlider.maxValue = baseHealth;
			healthSlider.value = baseHealth;
		}
    }

	void Start()
	{
		lastSpawnPoint = transform.position;
	}

    #region Event Listeners

    void onControllerCollider(RaycastHit2D hit)
    {
    }

    void onTriggerEnterEvent(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy" && Enemy.instakillOnTouch)
            this.transform.position = new Vector2(spawner.spawnPoint.transform.position.x, spawner.spawnPoint.transform.localPosition.y);

        if (col.gameObject.tag == "SpawnPoint")
            spawner.spawnPoint.transform.localPosition = col.transform.localPosition;
        //Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
    }

    void onTriggerExitEvent(Collider2D col)
    {
        //Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
    }
    #endregion

	public override void ModifyHealth (int amount)
	{
		base.ModifyHealth (amount);
		if (healthSlider)
			healthSlider.value = health;
	}

	protected override void SetHealth (int health)
	{
		base.SetHealth (health);
		if (healthSlider)
			healthSlider.value = health;
	}

	protected override void OnDie ()
	{
		transform.position = lastSpawnPoint;
		SetHealth (baseHealth);
		FindObjectOfType<Boss> ().Heal ();
	}

    // the Update loop only gathers input. Actual movement is handled in FixedUpdate because we are using the Physics system for movement
    void Update()
    {
        // a minor bit of trickery here. FixedUpdate sets _up to false so to ensure we never miss any jump presses we leave _up
        // set to true if it was true the previous frame
        _up = _up || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space);
        _right = Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
        _left = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
        _fire = Input.GetKeyDown(KeyCode.Mouse0);

		if (Input.GetKeyDown (KeyCode.Escape))
			Application.LoadLevel ("Main");
    }

    void FixedUpdate()
    {
        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;

        if (fireCooldown > 1)
            fireCooldown -= fireCooldown * Time.deltaTime;
        else
            hasFired = false;

        if (_controller.isGrounded)
            _velocity.y = 0;

        // we can only jump whilst grounded
        if (_controller.isGrounded && _up)
        {
            _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            _animator.Play(Animator.StringToHash("Jump"));
            hasJumped = true;
        }

        if (isDoubleJump && !_controller.isGrounded && _up && hasJumped)
        {
            _velocity.y = Mathf.Sqrt(2f * doubleJumpHeight * -gravity);
            _animator.Play(Animator.StringToHash("Jump"));
            hasJumped = false;
        }

        // apply horizontal speed smoothing it
        float smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.fixedDeltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.fixedDeltaTime;
        _controller.move(_velocity * Time.fixedDeltaTime);

        // reset input
        _up = false;
        Movement();
        GetFire();
        KillBox();
    }

    void Movement()
    {
        if (_right)
        {
            lastDirectionLeft = false;
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));

        }
        else if (_left)
        {
            lastDirectionLeft = true;
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));
        }
        else
        {

            normalizedHorizontalSpeed = 0;
            if (_controller.isGrounded && animationDelay != true)
            {
                _animator.Play(Animator.StringToHash("Idle"));
                //Debug.Log("Is grounded?" + _controller.isGrounded);
                //if (_animator.GetAnimatorTransitionInfo(0).IsName("Idle"))
                //    Debug.Log("Is grounded?" + _controller.isGrounded + "Playing animation grounded");

            }
        }
    }

    IEnumerator AnimationDelay(float waitTime)
    {
        animationDelay = true;
        yield return new WaitForSeconds(waitTime);
        animationDelay = false;
    }

    void GetFire()
    {
        if (lastDirectionLeft && _fire)
        {
            if (fireCooldown <= 1 && !hasFired)
            {
                if (_controller.isGrounded && _fire)
                {
                    _animator.Play(Animator.StringToHash("ShootRun"));
                    StartCoroutine(AnimationDelay(animationDelayTimer.shootAnimationDelayTimer));
                }
                if (!_controller.isGrounded && _fire)
                    _animator.Play(Animator.StringToHash("ShootJump"));
                Rigidbody2D projectile = (Rigidbody2D)Instantiate(bullet, new Vector2(this.transform.position.x - bulletOffset, this.transform.position.y), transform.rotation);
                projectile.GetComponent<Rigidbody2D>().AddForce(-Vector2.right * bulletSpeed * Time.deltaTime);
				projectile.GetComponent<Bullet> ().shouldHarmPlayer = false;
				hasFired = true;
                fireCooldown = fireRate;
            }
        }
        else if (!lastDirectionLeft && _fire)
        {
            if (fireCooldown <= 1 && !hasFired)
            {
                if (_controller.isGrounded && _fire)
                {
                    _animator.Play(Animator.StringToHash("ShootRun"));
                    StartCoroutine(AnimationDelay(animationDelayTimer.shootAnimationDelayTimer));
                }
                if (!_controller.isGrounded && _fire)
                    _animator.Play(Animator.StringToHash("ShootJump"));
                Rigidbody2D projectile = (Rigidbody2D)Instantiate(bullet, new Vector2(this.transform.position.x + bulletOffset, this.transform.position.y), transform.rotation);
                projectile.GetComponent<Rigidbody2D>().AddForce(Vector2.right * bulletSpeed * Time.deltaTime);
				projectile.GetComponent<Bullet> ().shouldHarmPlayer = false;
                hasFired = true;
                fireCooldown = fireRate;
            }
        }
    }

    void KillBox()
    {
        if (this.transform.localPosition.y <= killBox.theKillBox)
            this.transform.position = new Vector2(spawner.spawnPoint.transform.position.x, spawner.spawnPoint.transform.localPosition.y + spawner.spawnOffSet);
    }
}