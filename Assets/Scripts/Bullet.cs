﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
    [SerializeField]
    private int timer;
    private int tTimer;
    [SerializeField]
    private int bulletDamage;

	[HideInInspector]
	public bool shouldHarmPlayer = true;

	void Awake()
	{
		foreach(Collider2D c in GameManager.instance.omgs)
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), c);
	}

	// Use this for initialization
	void Start () 
    {
        if (timer == 0)
            timer = 300;
        if (bulletDamage == 0)
            bulletDamage = 1;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        tTimer++;
        if (tTimer >= timer)
            Destroy(this.gameObject);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        other.gameObject.SendMessage("ModifyHealth", -bulletDamage, SendMessageOptions.DontRequireReceiver);

		if(!other.gameObject.name.Contains("OMG"))
        	Destroy(this.gameObject);
    }

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag != "Player" || shouldHarmPlayer)
			other.gameObject.SendMessage("ModifyHealth", -bulletDamage, SendMessageOptions.DontRequireReceiver);
		if(!(other.gameObject.name.Contains("Bullet") && name.Contains("Fire")))
			if(!other.gameObject.name.Contains("OMG"))
				Destroy (this.gameObject);
	}
}
