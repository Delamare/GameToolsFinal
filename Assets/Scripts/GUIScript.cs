﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour
{
	public void LoadLevel(string level)
	{
		Application.LoadLevel (level);
	}

	public void Quit()
	{
		Application.Quit ();
	}
}
